<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<%--http://localhost:8080/crm/--%>
<%--也可以采用先对路径，但相对路径是先对当前页面而言的，要到jquery等出需要/../../..退三下--%>
<html>
<head>
	<base href="<%=basePath%>">
<meta charset="UTF-8">
<%--jquery等资源之所以放在外面是因为，这些资源跟数据无关，并且这些资源放在外面不受到保护，更容易进行引用--%>
<%--假设放到了WEB-INF下面，那么就连自己也要通过controller进行访问，相当于自己为难自己--%>
<link href="jquery/bootstrap_3.3.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="jquery/jquery-1.11.1-min.js"></script>
<script type="text/javascript" src="jquery/bootstrap_3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(function(){

		$(window).keydown(function (event) {
			if (event.keyCode ==13) {
				$("#but").click()
			}
		})
		 $("#but").click(function(){
			 var loginAct=$.trim($("#loginAct").val());
			 var loginPwd=$.trim($("#loginPwd").val());
			 var loginRem=$("#loginRem").prop("checked");


			 if(loginAct==""){
				 alert("用户名不能为空");
				 return;
			 }
			 if(loginPwd==""){
				alert("账号不能为空");
				return;
			 }

			 $.ajax({
				 url:'settings/qx/user/login.do',
				 data:{
					 loginAct:loginAct,
					 loginPwd:loginPwd,
					 loginRem:loginRem
				 },
				 type:'post',
				 dataType:'json',
				 success:function (data) {
					 if(data.code=="1"){
						 window.location.href="workbench/index.do";
					 }else {
						 $("#msg").text(data.message);
					 }
				 },
				 beforeSend:function () {
					 //ajax执行之前执行，返回ture则继续执行ajax操作，否则不发送ajax请求
					 $("#msg").text("正在加载中....")
					 return true;
				 }
			 })
		 })
	})
</script>
</head>
<body>
	<div style="position: absolute; top: 0px; left: 0px; width: 60%;">
		<img src="image/IMG_7114.JPG" style="width: 100%; height: 90%; position: relative; top: 50px;">
	</div>
	<div id="top" style="height: 50px; background-color: #3C3C3C; width: 100%;">
		<div style="position: absolute; top: 5px; left: 0px; font-size: 30px; font-weight: 400; color: white; font-family: 'times new roman'">CRM &nbsp;<span style="font-size: 12px;">&copy;2019&nbsp;动力节点</span></div>
	</div>
	
	<div style="position: absolute; top: 120px; right: 100px;width:450px;height:400px;border:1px solid #D5D5D5">
		<div style="position: absolute; top: 0px; right: 60px;">
			<div class="page-header">
				<h1>登录</h1>
			</div>
			<form action="workbench/index.html" class="form-horizontal" role="form">
				<div class="form-group form-group-lg">
					<div style="width: 350px;">
						<input id="loginAct" class="form-control" type="text" value="${cookie.loginAct.value}"  placeholder="用户名">
					</div>
					<div style="width: 350px; position: relative;top: 20px;">
						<input id="loginPwd" class="form-control" type="password" value="${cookie.loginPwd.value}" placeholder="密码">
					</div>
					<div class="checkbox"  style="position: relative;top: 30px; left: 10px;">
						<label>
							<c:if test="${not empty cookie.loginAct and not empty cookie.loginPwd}">
								<input id="loginRem" type="checkbox" checked>
							</c:if>
							<c:if test="${empty cookie.loginAct and empty cookie.loginPwd}">
								<input id="loginRem" type="checkbox">
							</c:if>
							 十天内免登录
						</label>
						&nbsp;&nbsp;
						<span id="msg" style="color: red"></span>
					</div>
					<button type="button" id="but" class="btn btn-primary btn-lg btn-block"  style="width: 350px; position: relative;top: 45px;">登录</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>