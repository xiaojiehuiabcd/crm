package com.xjh.crm.workbench.service;

import com.xjh.crm.workbench.bean.Activity;

import java.util.List;
import java.util.Map;

public interface ActivityService {
    int saveCreateActivity(Activity activity);

    List<Activity> selectActivityByConditionForPage(Map<String,Object> map);

    int selectCountOfActivityByCondition(Map<String,Object> map);

    int deleteActivityByIds(String[] ids);

    Activity queryActivityById(String id);

    int updateActivity(Activity activity);

    List<Activity> exportAllActivity(Activity activity);

    List<Activity> exportByIdActivity(String[] id);

    int insertImportByIdActivity(List<Activity> activityList);
}
