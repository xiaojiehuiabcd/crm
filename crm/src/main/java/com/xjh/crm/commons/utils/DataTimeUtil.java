package com.xjh.crm.commons.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataTimeUtil {

    private DataTimeUtil(){}

    public static String dataTimeFormat(Date date){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    public static String dataFormat(Date date){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String TimeFormat(Date date){
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        return sdf.format(date);
    }

}
