package com.xjh.crm.commons.bean;

public class ReturnObject {
    private String code;
    private String message;
    private Object rest;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getRest() {
        return rest;
    }

    public void setRest(Object rest) {
        this.rest = rest;
    }
}
