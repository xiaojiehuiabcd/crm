package com.xjh.crm.workbench.controller;

import com.mysql.cj.x.protobuf.MysqlxCrud;
import com.xjh.crm.commons.bean.ActivityReturn;
import com.xjh.crm.commons.bean.ReturnObject;
import com.xjh.crm.commons.constant.Constant;
import com.xjh.crm.commons.utils.DataTimeUtil;
import com.xjh.crm.commons.utils.HSSFUtils;
import com.xjh.crm.commons.utils.UUIDUtils;
import com.xjh.crm.settings.bean.User;
import com.xjh.crm.settings.service.UserService;
import com.xjh.crm.workbench.bean.Activity;
import com.xjh.crm.workbench.service.ActivityService;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;

import static com.xjh.crm.commons.constant.Constant.SESSESION_USER;

@Controller
public class ActivityController {

    @Autowired
   private UserService userService;
    
    @Autowired
    private ActivityService activityService;

    @RequestMapping("/workbench/activity/index.do")
    public String index(HttpServletRequest request){
        List<User> userList = userService.queryAllUsers();
        request.setAttribute("userList",userList);
        return "workbench/activity/index";
    }
    
    @RequestMapping("/workbench/activity/saveCreateActivity")
    @ResponseBody
    public Object saveCreateActivity(Activity act, HttpSession session){
        User user = (User) session.getAttribute(SESSESION_USER);
        ReturnObject retObj=new ReturnObject();

        act.setId(UUIDUtils.getUUID());
        act.setCreateBy(user.getId());
        act.setCreateTime(DataTimeUtil.dataTimeFormat(new Date()));

        try {
            int i = activityService.saveCreateActivity(act);
            if (i>0) {
                retObj.setCode(Constant.RETURN_CODE_SUCCESS);
            }else {
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("网络繁忙，请稍后....");
            }
        } catch (Exception e) {
            e.printStackTrace();
            retObj.setCode(Constant.RETURN_CODE_FIAL);
            retObj.setMessage("网络繁忙，请稍后....");
        }

        return retObj;
    }

    @RequestMapping("/workbench/activity/queryActivityByConditionForPage.do")
    @ResponseBody
    public Object queryActivityByConditionForPage(String name,String owner,String startDate,String endDate,
                                                  Integer pageNo,Integer pageSize){
        Map<String,Object> map=new HashMap<>();
        map.put("name",name);
        map.put("owner",owner);
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        map.put("beginNo",(pageNo-1)*pageSize);
        map.put("pageSize",pageSize);
        List<Activity> activities = activityService.selectActivityByConditionForPage(map);
        int number = activityService.selectCountOfActivityByCondition(map);
        return new ActivityReturn(activities,number);
    }

    @RequestMapping("/workbench/activity/deleteActivityByIds.do")
    @ResponseBody
    public Object deleteActivityByIds(String[] id){
        ReturnObject retObj = new ReturnObject();
        try {
            int i = activityService.deleteActivityByIds(id);
            if (i!=0) {
                retObj.setCode(Constant.RETURN_CODE_SUCCESS);
            }else{
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("系统繁忙，请稍后再试......");
            }
        } catch (Exception e) {
             e.printStackTrace();
            retObj.setCode(Constant.RETURN_CODE_FIAL);
            retObj.setMessage("系统繁忙，请稍后再试......");
        }
        return retObj;
    }

    @RequestMapping("/workbench/activity/selectActivityById.do")
    @ResponseBody
    public Object selectActivityById(String id){
        Activity activity = activityService.queryActivityById(id);
        return activity;
    }

    @RequestMapping("/workbench/activity/updateActivity.do")
    @ResponseBody
    public Object updateActivity(HttpSession session,Activity activity){
        ReturnObject retObj = new ReturnObject();

        User user = (User) session.getAttribute(SESSESION_USER);
        String editBy = user.getId();
        String editTime = DataTimeUtil.dataTimeFormat(new Date());
        activity.setEditBy(editBy);
        activity.setEditTime(editTime);
        try {
            int i = activityService.updateActivity(activity);
            if(i>0){
                retObj.setCode(Constant.RETURN_CODE_SUCCESS);
            }else{
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("系统加载中，请稍后再试...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            retObj.setCode(Constant.RETURN_CODE_FIAL);
            retObj.setMessage("系统加载中，请稍后再试...");
        }
        return retObj;
    }

    @RequestMapping("/workbench/activity/exportAllActivity.do")
    public void exportAllActivity(HttpServletResponse response,Activity activity)throws IOException{
        List<Activity> activities = activityService.exportAllActivity(activity);

//      创建excel表（或者说是工作簿），相当于未保存状态
        HSSFWorkbook wb=new HSSFWorkbook();

//      使用excel设置样式，但具体谁使用还未设置
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER); //文字居中

//      设置创建excel表中的页，页名为“活动列表”
        HSSFSheet sheet = wb.createSheet("活动列表");

//      设置页中的行，并且要标明是第几行（注意：行和列都是从0开始逐级递增的，所以0就是表示excel表中的第一行）
        HSSFRow row = sheet.createRow(0);
//      设置行中的列（或单元格），并且标明是第几列
        HSSFCell cell = row.createCell(0);

//      设置第一行样式居中，也可以单独设置列的样式
        row.setRowStyle(style);

        cell.setCellValue("ID");
        cell = row.createCell(1); //这里之所以可以继续使用cell不怕被覆盖上一个是因为，其后端代码帮我们弄好了
        cell.setCellValue("所有者");
        cell=row.createCell(2);
        cell.setCellValue("用户");
        cell=row.createCell(3);
        cell.setCellValue("开始时间");
        cell=row.createCell(4);
        cell.setCellValue("结束时间");
        cell=row.createCell(5);
        cell.setCellValue("预计");
        cell=row.createCell(6);
        cell.setCellValue("描述");
        cell=row.createCell(7);
        cell.setCellValue("创建时间");
        cell=row.createCell(8);
        cell.setCellValue("创建者");
        cell=row.createCell(9);
        cell.setCellValue("修改时间");
        cell=row.createCell(10);
        cell.setCellValue("修改者");

        if(activities!=null && activities.size()!=0){
            Activity act=null;
            for (int i=0;i<activities.size();i++){
                act=activities.get(i);
                row = sheet.createRow(i+1);

                cell = row.createCell(0);
                cell.setCellValue(act.getId());
                cell = row.createCell(1); //这里之所以可以继续使用cell不怕被覆盖上一个是因为，其后端代码帮我们弄好了
                cell.setCellValue(act.getOwner());
                cell=row.createCell(2);
                cell.setCellValue(act.getName());
                cell=row.createCell(3);
                cell.setCellValue(act.getStartDate());
                cell=row.createCell(4);
                cell.setCellValue(act.getEndDate());
                cell=row.createCell(5);
                cell.setCellValue(act.getCost());
                cell=row.createCell(6);
                cell.setCellValue(act.getDescription());
                cell=row.createCell(7);
                cell.setCellValue(act.getCreateTime());
                cell=row.createCell(8);
                cell.setCellValue(act.getCreateBy());
                cell=row.createCell(9);
                cell.setCellValue(act.getEditTime());
                cell=row.createCell(10);
                cell.setCellValue(act.getEditBy());
            }
        }
//      前面目录没有会报错，但后面activity.xls没有会补全
//        FileOutputStream fos = new FileOutputStream("C:\\Users\\86155\\Desktop\\excel\\activity.xls");
//        wb.write(fos);

//      这里wb也是需要关闭，因为其实生成在java段，也很耗费内存
//        fos.close();
//        wb.close();

//      响应的状态，是以文本还是二进制返回到浏览器
//      应用文本/二进制
        response.setContentType("application/octet-stream;charset=UTF-8");
//      这里为什么不捕获呢，是因为这里、response返回的信息不是返回到主页面的而是返回到下载窗口的，所以即使捕获了前台也不会显示
        OutputStream out = response.getOutputStream();

//      这里是最重要的，表示以什么样的形式展现出来，这里表示以附件形式展现(attachment),至于Content-Disposition
//      是用于设置的展现形式的，一般默认浏览器主页面展示出来
//      filename这个值一定要设置,因为其会告诉前端（这是一个什么文件），文件名叫什么
        response.addHeader("Content-Disposition","attachment;filename=stu.xls");
//        InputStream is = new FileInputStream("C:\\Users\\86155\\Desktop\\excel\\activity.xls");
//
//        byte[] bytes=new byte[256];
//        int count=0;
//        while ((count=is.read(bytes))!=-1){
//            out.write(bytes,0,count);
//        }
//
//        is.close();
        wb.write(out);
        wb.close();
        out.flush();
    }


    @RequestMapping("/workbench/activity/exportByIdActivity.do")
    public void exportByIdActivity(HttpServletResponse response,String[] id)throws IOException{
        List<Activity> activities = activityService.exportByIdActivity(id);

//      创建excel表（或者说是工作簿），相当于未保存状态
        HSSFWorkbook wb=new HSSFWorkbook();

//      使用excel设置样式，但具体谁使用还未设置
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER); //文字居中

//      设置创建excel表中的页，页名为“活动列表”
        HSSFSheet sheet = wb.createSheet("活动列表");

//      设置页中的行，并且要标明是第几行（注意：行和列都是从0开始逐级递增的，所以0就是表示excel表中的第一行）
        HSSFRow row = sheet.createRow(0);
//      设置行中的列（或单元格），并且标明是第几列
        HSSFCell cell = row.createCell(0);

        cell.setCellValue("ID");
        cell = row.createCell(1); //这里之所以可以继续使用cell不怕被覆盖上一个是因为，其后端代码帮我们弄好了
        cell.setCellValue("所有者");
        cell=row.createCell(2);
        cell.setCellValue("用户");
        cell=row.createCell(3);
        cell.setCellValue("开始时间");
        cell=row.createCell(4);
        cell.setCellValue("结束时间");
        cell=row.createCell(5);
        cell.setCellValue("预计");
        cell=row.createCell(6);
        cell.setCellValue("描述");
        cell=row.createCell(7);
        cell.setCellValue("创建时间");
        cell=row.createCell(8);
        cell.setCellValue("创建者");
        cell=row.createCell(9);
        cell.setCellValue("修改时间");
        cell=row.createCell(10);
        cell.setCellValue("修改者");

        if(activities!=null && activities.size()!=0){
            Activity act=null;
            for (int i=0;i<activities.size();i++){
                act=activities.get(i);
                row = sheet.createRow(i+1);

                cell = row.createCell(0);
                cell.setCellValue(act.getId());
                cell = row.createCell(1); //这里之所以可以继续使用cell不怕被覆盖上一个是因为，其后端代码帮我们弄好了
                cell.setCellValue(act.getOwner());
                cell=row.createCell(2);
                cell.setCellValue(act.getName());
                cell=row.createCell(3);
                cell.setCellValue(act.getStartDate());
                cell=row.createCell(4);
                cell.setCellValue(act.getEndDate());
                cell=row.createCell(5);
                cell.setCellValue(act.getCost());
                cell=row.createCell(6);
                cell.setCellValue(act.getDescription());
                cell=row.createCell(7);
                cell.setCellValue(act.getCreateTime());
                cell=row.createCell(8);
                cell.setCellValue(act.getCreateBy());
                cell=row.createCell(9);
                cell.setCellValue(act.getEditTime());
                cell=row.createCell(10);
                cell.setCellValue(act.getEditBy());
            }
        }

//      响应的状态，是以文本还是二进制返回到浏览器
//      应用文本/二进制
        response.setContentType("application/octet-stream;charset=UTF-8");
//      这里为什么不捕获呢，是因为这里、response返回的信息不是返回到主页面的而是返回到下载窗口的，所以即使捕获了前台也不会显示
        OutputStream out = response.getOutputStream();
        response.addHeader("Content-Disposition","attachment;filename=stu.xls");

        wb.write(out);
        wb.close();
        out.flush();
    }


    @RequestMapping("/workbench/activity/importByIdActivity.do")
    @ResponseBody
    public Object insertImportByIdActivity(MultipartFile multipartFile,HttpSession session){
        ReturnObject ret=new ReturnObject();
        try {
//          上传的文件本身就具有输入流，所以不需要在transferTo(new File("路劲"))（使用FileOutPutStream）将其输入到服务器端，
//          在进行文件输入流到new HSSFWorkbook(InputStream is)
            InputStream is = multipartFile.getInputStream();
            HSSFWorkbook wb = new HSSFWorkbook(is);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row =null;
            HSSFCell cell=null;

            List<Activity> activityList=new ArrayList<>();
            Activity activity=null;
            for(int i=1;i<=sheet.getLastRowNum();i++){
                activity=new Activity();
                activity.setId(UUIDUtils.getUUID());
                User user =(User) session.getAttribute(SESSESION_USER);
                activity.setOwner(user.getId());
                activity.setCreateTime(DataTimeUtil.dataTimeFormat(new Date()));
                activity.setCreateBy(user.getId());

                row = sheet.getRow(i);
                for(int j=0;j<row.getLastCellNum();j++){
                    cell = row.getCell(j);
                    String cellValue = HSSFUtils.getCellValue(cell);
                    if (j==0){
                        activity.setName(cellValue);
                    }else if(j==1){
                        activity.setStartDate(cellValue);
                    }else if(j==2){
                        activity.setEndDate(cellValue);
                    }else if (j==3){
                        activity.setCost(cellValue);
                    } else if (j==4) {
                        activity.setDescription(cellValue);
                    }
                }
                activityList.add(activity);
            }

            int i = activityService.insertImportByIdActivity(activityList);
            if (i>0) {
                ret.setCode(Constant.RETURN_CODE_SUCCESS);
            }else {
                ret.setCode(Constant.RETURN_CODE_FIAL);
                ret.setMessage("系统繁忙，请稍后再试......");
            }

        } catch (IOException e) {
            ret.setCode(Constant.RETURN_CODE_FIAL);
            ret.setMessage("系统繁忙，请稍后再试......");
        }
        return ret;
    }

    @RequestMapping("/importByIdActivity.do")
    @ResponseBody
    public Object simportByIdActivity1(String name,MultipartFile multipartFile) throws Exception{
        Activity activity=new Activity();
        String filename = multipartFile.getOriginalFilename();
        multipartFile.transferTo(new File("C:\\Users\\86155\\Desktop\\excel\\import\\"+filename));

        InputStream is = new FileInputStream("C:\\Users\\86155\\Desktop\\excel\\import\\"+filename);
        HSSFWorkbook wb = new HSSFWorkbook(is);
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row=null;
        HSSFCell cell=null;
//       sheet.getLastRowNum() 这个获取的是最后一行的下标
        for (int j=0;j<=sheet.getLastRowNum();j++){
            row = sheet.getRow(j);
//       row.getLastCellNum()这个获取的是这列的长度，或者说是最后一一列下标加1
            for(int i=0;i<row.getLastCellNum();i++){
                cell = row.getCell(i);
                String cellValue = HSSFUtils.getCellValue(cell);
                System.out.printf(cellValue+" ");
            }
            System.out.println("");
        }


        Map map=new HashMap<>();
        map.put("name",name);
        map.put("filename",filename);
        map.put("xing","xiao");
        map.put("getLastRowNum",sheet.getLastRowNum());
        map.put("getLastCellNum",sheet.getRow(1).getLastCellNum());
        return map;
    }



}
