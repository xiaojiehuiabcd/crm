<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page session="true" %>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<script type="text/javascript">
		<%--之所以这里可以运行是因为采用了相对路径（wabapp目录为跟目录）前面会自动补全http://127.0.0.1:8080/crm/WEB-INF/pages/--%>
		<%--因为使用了相对路径前面绝对不能写/不然会造成双斜杠//致使路径错误--%>
		document.location.href = "settings/qx/user/toLogin.do";
	</script>
</body>
</html>