package com.xjh.crm.commons.utils;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class HSSFUtils {
    private HSSFUtils(){}

    public static String getCellValue(HSSFCell cell){
        String cellValue="";
        if(cell.getCellType()==HSSFCell.CELL_TYPE_STRING){
            cellValue=""+cell.getStringCellValue();
        }else if (cell.getCellType()==HSSFCell.CELL_TYPE_BOOLEAN){
            cellValue=""+cell.getBooleanCellValue();
        } else if (cell.getCellType()==HSSFCell.LAST_COLUMN_NUMBER) {
            cellValue=""+cell.getNumericCellValue();
        }else{
            cellValue="";
        }
        return cellValue;
    }
}
