package com.xjh.crm.commons.utils;

import java.util.UUID;

public class UUIDUtils {

    private UUIDUtils(){}

    /**
     * 这里的UUID之所以要加上toString()方法，是因为
     * UUID.randomUUID()方法生成的是一个随机数的内存地址，这里UUID的toString()进行了重写
     * 会生成带-的36位数，我们要把“-”替换为“”，变为我们熟悉的32位数
     * @return
     */
    public static String getUUID(){
       return UUID.randomUUID().toString().replaceAll("-","");
    }
}
