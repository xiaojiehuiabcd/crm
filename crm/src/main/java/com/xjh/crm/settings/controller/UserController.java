package com.xjh.crm.settings.controller;

import com.xjh.crm.commons.bean.ReturnObject;
import com.xjh.crm.commons.constant.Constant;
import com.xjh.crm.commons.utils.DataTimeUtil;
import com.xjh.crm.settings.bean.User;
import com.xjh.crm.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统管理的controller层
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 这里的路径是找那个页面所在的页面，但最后toLogin.do和login不能相同，因为要区别开来
     * 并且在超链接中默认路径从crm跟路径开始，和这里去掉/WEB-INF/pages/存在差别
     * 这里之所以去掉是因为默认全部路径都在/WEB-INF/pages/
     * @return
     */
    @RequestMapping("/settings/qx/user/toLogin.do")
    public String toLogin(){
        return "/settings/qx/user/login";
    }


    /**
     * 这个地方的路径和他返回信息的路径保持一致，资源名称和方法名一样（即返回到那个页面的路径）
     * 一般放回json的数据，返回值都用Object因为其具有通用性
     * @return
     */
    @RequestMapping("/settings/qx/user/login.do")
    @ResponseBody
    public Object login(String loginAct, String loginPwd, String loginRem, HttpServletResponse response,
                        HttpServletRequest request, HttpSession session){

        Map<String,Object> map=new HashMap<String, Object>();
        map.put("loginAct",loginAct);
        map.put("loginPwd",loginPwd);

        User user = userService.queryUserByLoginActAndPwd(map);

        ReturnObject retObj=new ReturnObject();
        if (user==null) {
//          账号或密码错误
            retObj.setCode(Constant.RETURN_CODE_FIAL);
            retObj.setMessage("账号或密码错误");
        }else{
             String nowTime= DataTimeUtil.dataTimeFormat(new Date());
            if(nowTime.compareTo(user.getExpireTime())>0){
//              账号已过时
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("账号已过时");
            }else if ("0".equals(user.getLockState())) {
//                账号被锁定
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("账号被锁定");
            }else if (!user.getAllowIps().contains(request.getRemoteAddr())){
//              不在规定的域名中
                retObj.setCode(Constant.RETURN_CODE_FIAL);
                retObj.setMessage("不在规定的域名中");
            } else {
                retObj.setCode(Constant.RETURN_CODE_SUCCESS);

                session.setAttribute(Constant.SESSESION_USER,user);
                if ("true".equals(loginRem)) {
                    Cookie c1 = new Cookie("loginAct", loginAct);
                    Cookie c2 = new Cookie("loginPwd", loginPwd);
                    c1.setMaxAge(10*24*60*60);
                    c2.setMaxAge(10*24*60*60);
                    response.addCookie(c1);
                    response.addCookie(c2);
                }

            }
        }
        return retObj;
    }


    @RequestMapping("/settings/qx/user/logout.do")
    public String logout(HttpServletResponse response,HttpSession session){
        Cookie c1 = new Cookie("loginAct", "1");
        Cookie c2 = new Cookie("loginPwd", "1");
        c1.setMaxAge(0);
        c2.setMaxAge(0);
        response.addCookie(c1);
        response.addCookie(c2);
        session.invalidate();
        return "redirect:/";
    }
}
