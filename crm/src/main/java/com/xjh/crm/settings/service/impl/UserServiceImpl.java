package com.xjh.crm.settings.service.impl;

import com.xjh.crm.settings.bean.User;
import com.xjh.crm.settings.mapper.UserMapper;
import com.xjh.crm.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 之所以这里给spring管理是因为使用了jdk动态代理
 * 那个包扫描实际上是一个创建目标对象的过程
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryUserByLoginActAndPwd(Map<String, Object> map) {
        return userMapper.selectUserByLoginActAndPwd(map);
    }

    @Override
    public List<User> queryAllUsers() {
        return userMapper.selectAllUsers();
    }
}
