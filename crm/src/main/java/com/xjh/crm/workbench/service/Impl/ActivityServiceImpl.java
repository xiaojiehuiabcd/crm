package com.xjh.crm.workbench.service.Impl;

import com.xjh.crm.workbench.bean.Activity;
import com.xjh.crm.workbench.mapper.ActivitieMapper;
import com.xjh.crm.workbench.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivitieMapper activitieMapper;

    @Override
    public int saveCreateActivity(Activity activity) {
        return activitieMapper.insertActivity(activity);
    }

    @Override
    public List<Activity> selectActivityByConditionForPage(Map<String, Object> map) {
        return activitieMapper.selectActivityByConditionForPage(map);
    }

    @Override
    public int selectCountOfActivityByCondition(Map<String, Object> map) {
        return activitieMapper.selectCountOfActivityByCondition(map);
    }

    @Override
    public int deleteActivityByIds(String[] ids){
        return activitieMapper.deleteActivityByIds(ids);
    }

    @Override
    public Activity queryActivityById(String id) {
        return activitieMapper.selectActivityById(id);
    }

    @Override
    public int updateActivity(Activity activity) {
        return activitieMapper.updateActivity(activity);
    }

    @Override
    public List<Activity> exportAllActivity(Activity activity){
        return activitieMapper.exportAllActivity(activity);
    }

    @Override
    public List<Activity> exportByIdActivity(String[] id){
        return activitieMapper.exportByIdActivity(id);
    }

    @Override
    public int insertImportByIdActivity(List<Activity> activityList) {
        return activitieMapper.insertImportByIdActivity(activityList);
    }
}
