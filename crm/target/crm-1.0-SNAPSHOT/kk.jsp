<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <link href="jquery/bootstrap_3.3.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="jquery/bs_pagination-master/css/jquery.bs_pagination.min.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="jquery/bs_pagination-master/css/jquery.bs_pagination.min.css">

    <script type="text/javascript" src="jquery/jquery-1.11.1-min.js"></script>
    <script type="text/javascript" src="jquery/bootstrap_3.3.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="jquery/bs_pagination-master/js/jquery.bs_pagination.min.js"/>
    <script type="text/javascript" src="jquery/bs_pagination-master/localization/en.js"/>
    <script type="text/javascript" src="jquery/bs_pagination-master/js/jquery.bs_pagination.min.js"></script>
    <script type="text/javascript" src="jquery/bs_pagination-master/localization/en.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $("#demo_pag1").bs_pagination({
                currentPage: 1,
                rowsPerPage: 3, //每页显示的行数
                totalRows: 9, //总记录条数
                totalPages: 3, //页数
                visiblePageLinks: 5, //下面显示的行数
                showGoToPage: true,  //是否有到某一页的画面
                showRowsPerPage: true, //是否有一页有多少记录页面
                showRowsInfo: true,  //是否有记录
                // onChangePage: function(event, page) { // returns page_num and rows_per_page after a link has clicked
                //     activityByConditionForPage(page.currentPage, page.rowsPerPage)
                // }
            });
        });

    </script>
</head>
<body>
<div id="demo_pag1"></div>
</body>