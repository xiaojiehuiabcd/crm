package com.xjh.crm.settings.service;

import com.xjh.crm.settings.bean.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    User queryUserByLoginActAndPwd(Map<String,Object> map);

    List<User> queryAllUsers();
}
