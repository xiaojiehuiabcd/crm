package com.xjh.crm.commons.bean;

import com.xjh.crm.workbench.bean.Activity;

import java.util.List;

public class ActivityReturn {
    private List<Activity> activities;

    private int number;

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ActivityReturn() {
    }

    public ActivityReturn(List<Activity> activities, int number) {
        this.activities = activities;
        this.number = number;
    }
}
